package org.cs2008.https;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;


/**
 * 验证HTTPS可用性
 */
public class HttpsValidator {

    static String USERNAME = "";
    static String PASSWORD = "";
    
    
    public static void main(String[] args) throws Throwable {
        //设置代理
        String proxy = "221.228.248.248";
        int port = 8085;
        String url="https://www.baidu.com";
//        String url="https://ss0.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/css/super_min_c29f78d5.css";
        System.out.println(executeGet(url, proxy, port));
        
        /*
        System.setProperty("proxyType", "4");
          System.setProperty("proxyPort", Integer.toString(port));
          System.setProperty("proxyHost", proxy);
          System.setProperty("proxySet", "true");
          
          
          SSLContext sc = SSLContext.getInstance("SSL");
            
          //指定信任https
          sc.init(null, new TrustManager[]{new TrustAnyTrustManager()}, new java.security.SecureRandom());
          URL console = new URL(url);
          HttpsURLConnection conn = (HttpsURLConnection) console.openConnection();
          conn.setSSLSocketFactory(sc.getSocketFactory());
          conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
          conn.connect();
          System.out.println("返回结果："+conn.getResponseMessage());
            
          InputStream is = conn.getInputStream();
          BufferedReader reader = new BufferedReader(new InputStreamReader(is));
          String curLine="";
          while ((curLine = reader.readLine()) != null) {
          System.out.println(curLine);
          }
          is.close();
          */
    }
    
    private static class TrustAnyTrustManager implements X509TrustManager {

        public void checkClientTrusted(
                java.security.cert.X509Certificate[] chain, String authType)
                throws java.security.cert.CertificateException {
            // TODO Auto-generated method stub

        }

        public void checkServerTrusted(
                java.security.cert.X509Certificate[] chain, String authType)
                throws java.security.cert.CertificateException {
            // TODO Auto-generated method stub

        }

        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            // TODO Auto-generated method stub
            return null;
        }
    }
    
    private static class TrustAnyHostnameVerifier implements HostnameVerifier {
        public boolean verify(String hostname, SSLSession session) {
        return true;
        }
    }
    
    private static String executeGet(final String https_url, final String proxyName, final int port) {
        String ret = "";

        URL url;
        try {

            HttpsURLConnection con;
            url = new URL(https_url);

            if (proxyName.isEmpty()) {  
                con = (HttpsURLConnection) url.openConnection();
            } else {                
                Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyName, port));
                con = (HttpsURLConnection) url.openConnection(proxy);
                Authenticator authenticator = new Authenticator() {
                    public PasswordAuthentication getPasswordAuthentication() {
                            return (new PasswordAuthentication(USERNAME, PASSWORD.toCharArray()));
                        }
                    };
                Authenticator.setDefault(authenticator);
            }

            ret = getContent(con);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ret;
    }

    private static String getContent(HttpsURLConnection conn) {
        try {
            InputStream is = conn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String curLine = "";
            while ((curLine = reader.readLine()) != null) {
                sb.append(curLine).append("\n");
            }
            is.close();
            return sb.toString();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
}
