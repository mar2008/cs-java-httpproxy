package org.cs2008.httpproxy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import javax.net.ServerSocketFactory;

/**
 * Hello world!
 *
 */
public class HTTPProxyDemo 
{
    public static void main( String[] args )
    {
    	int port = 7088;
    	try {
			ServerSocket ss = ServerSocketFactory.getDefault().createServerSocket(port);
			while(true){
				Socket s = ss.accept();
				InputStream is = s.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String str = null;
				StringBuilder sb = new StringBuilder();
				int c = 0;
				while((c = br.read()) != -1){
					System.out.println(1);
				}
				System.out.println(sb.toString());
				br.close();
				OutputStream os = s.getOutputStream();
				os.write("hello".getBytes());
				os.flush();
				os.close();
				s.close();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
        System.out.println( "Hello World!" );
    }
}
